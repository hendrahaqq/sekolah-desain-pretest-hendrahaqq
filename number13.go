package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Todo struct {
	id   string   `json:"Id"`
	Date string   `json:"date"`
	List []string `json:"list"`
}

var Todos []Todo

func returnAllTodos(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(Todos)
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", returnAllTodos)

	log.Fatal(http.ListenAndServe(":8880", myRouter))
}

func main() {
	Todos = []Todo{
		Todo{id: "1", Date: "22-10-2020", List: []string{"Mandi", "Makan", "Tidur"}},
		Todo{id: "2", Date: "23-22-2020", List: []string{"Tidur", "Mandi", "Makan"}},
	}
	handleRequests()
}
