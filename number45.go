package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Nama  string `json:"firstName"`
	Akhir string `json:"lastName"`
	Umur  int    `json:"age"`
	Email string `json:"email"`
}

func main() {

	user := User{Nama: "Muhammad", Akhir: "Hendra", Umur: 22, Email: "hendrahaqq@gmail.com"} //nomor 4

	byteArray, err := json.Marshal(user)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(byteArray)) // nomor5
}
