package main

import (
	"fmt"
	"log"
	"regexp"
	"strings"
)

func reverse(s string) string { //membalik kalimat
	rs := []rune(s)
	for i, j := 0, len(rs)-1; i < j; i, j = i+1, j-1 {
		rs[i], rs[j] = rs[j], rs[i]
	}
	return string(rs)
}

func main() {

	text := strings.ToUpper("Mr. Owl ate my metal worm") //insert text here
	re, err := regexp.Compile(`[^\w]`)                   //hapus tanda baca
	if err != nil {
		log.Fatal(err)
	}
	text = re.ReplaceAllString(text, " ")
	noSpace := strings.Replace(text, " ", "", -1) //hapus spasi

	if reverse(noSpace) == noSpace { //if palindrom
		fmt.Println("Jos Gandoss, Ini palindrom")
	} else {
		fmt.Println("waduh, bukan palindrom")
	}

}
